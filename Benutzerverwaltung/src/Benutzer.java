import java.text.SimpleDateFormat;
import java.util.Date;

public class Benutzer {

	private int bnummer;
	private String vname;
	private String nname;
	private int geburtsjahr;
	
	public Benutzer() {
		this.bnummer = 0;
		this.vname = null;
		this.nname = null;
		this.geburtsjahr = 0;
	}

	public Benutzer(int bnummer, String vname, String nname, int geburtsjahr) {
		this.bnummer = bnummer;
		this.vname = vname;
		this.nname = nname;
		this.geburtsjahr = geburtsjahr;
		
	}

	public void setBNummer(int bnummer) {
		this.bnummer = bnummer;
	}

	public int getBNummer() {
		return bnummer;
	}

	public void setVName(String vname) {
		this.vname = vname;
	}

	public String getVName() {
		return vname;
	}

	public void setNName(String nname) {
		this.nname = nname;
	}

	public String getNName() {
		return nname;
	}

	public void setGeburtsjahr(int geburtsjahr) {
		this.geburtsjahr = geburtsjahr;
	}

	public int getGeburtsjahr() {
		return geburtsjahr;
	}
	
	public int getAlter() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		Date date = new Date();
		int aYear = Integer.parseInt(formatter.format(date));
		int alter = aYear - geburtsjahr;
		return alter;
	}
}
