import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {

	public static void main(String[] args) {
		
		ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();
		
		Benutzer b1 = new Benutzer(1001, "Max", "Mustermann", 2000);
		
		benutzerliste.add(b1);
		
		int auswahl;
		
		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				// benutzerAnzeigen();
				System.out.println(benutzerliste);
				break;
			case 2:
				benutzerErfassen(benutzerliste);
				

				break;
			case 3:
				// benutzerLoeschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");				
			}
		}while (true );
		
	}
	
	public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("     Benutzerverwaltung     ");
        System.out.println("----------------------------\n");
        System.out.println("1 - Benutzer anzeigen");
        System.out.println("2 - Benutzer erfassen");
        System.out.println("3 - Benutzer l�schen");
        System.out.println("4 - Ende");
         
        System.out.print("\nEingabe:  ");
        selection = input.nextInt();
        return selection;    
    }
	
	public static void benutzerErfassen(ArrayList <Benutzer> benutzerliste) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Benutzernummer ein.");
		int bnummer = tastatur.nextInt();
		System.out.println("Bitte geben Sie den Vornamen des Benutzers ein.");
		String vname = tastatur.next();
		System.out.println("Bitte geben Sie den Nachnamen des Benutzers ein.");
		String nname = tastatur.next();
		System.out.println("Bitte geben Sie das Geburtsjahr des Benutzers ein.");
		int geburtsjahr = tastatur.nextInt();
		Benutzer b2 = new Benutzer(bnummer, vname, nname, geburtsjahr);
		benutzerliste.add(b2);
	}
	

}