import java.util.Scanner;

public class SchleifenBeispiele {

	public static void main(String[] args) {
		
		/*
		System.out.println("Geben Sie bitte eine Zahl ein:");
		Scanner tastatur = new Scanner(System.in);
		int eingabezahl = tastatur.nextInt();
		
		int zaehler = 1;
		while (zaehler <= eingabezahl) {
			System.out.print(zaehler + " ");
			zaehler++;
			
		}
		*/
		
		System.out.println("Geben Sie bitte eine Zahl ein:");
		Scanner tastatur = new Scanner(System.in);
		int eingabezahl = tastatur.nextInt();
		
		int zaehler = 1;
		int ergebnis = 0;
		
		while (zaehler <= eingabezahl) {
			
			System.out.print(zaehler);
			
			if (zaehler == eingabezahl) {
				System.out.print(" = ");
			}
			else {
				System.out.print(" + ");
			}
			ergebnis = ergebnis + zaehler;
			zaehler++;
		}
		
		System.out.print(ergebnis);
		
	}

}
