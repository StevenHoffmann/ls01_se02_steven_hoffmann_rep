
public class LadungTest {
	
	public static void main(String[] args) {
		
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 2);
		Ladung l4 = new Ladung("Forschungssonden", 35);
		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 20);
		Ladung l6 = new Ladung("Plasma-Waffe", 50);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		
		System.out.println("Objekt L1:");
		System.out.println(l1.getBezeichnung());
		System.out.println(l1.getMenge());
		System.out.println();
		
		System.out.println("Objekt L2:");
		System.out.println(l2.getBezeichnung());
		System.out.println(l2.getMenge());
		System.out.println();
		
		System.out.println("Objekt L3:");
		System.out.println(l3.getBezeichnung());
		System.out.println(l3.getMenge());
		System.out.println();
		
		System.out.println("Objekt L4:");
		System.out.println(l4.getBezeichnung());
		System.out.println(l4.getMenge());
		System.out.println();
		
		System.out.println("Objekt L5:");
		System.out.println(l5.getBezeichnung());
		System.out.println(l5.getMenge());
		System.out.println();
		
		System.out.println("Objekt L6:");
		System.out.println(l6.getBezeichnung());
		System.out.println(l6.getMenge());
		System.out.println();
		
		System.out.println("Objekt L7:");
		System.out.println(l7.getBezeichnung());
		System.out.println(l7.getMenge());
		System.out.println();

	}

}
