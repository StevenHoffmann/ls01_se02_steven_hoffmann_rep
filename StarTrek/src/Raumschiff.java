import java.util.Scanner;

public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private String broadcastKommunikator[];
	private String ladungsverzeichnis[];
	int raumschiffeAnzahl[] = {1,2,3};
	
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsname = "Unbekannt";

		
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent,
			int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

// Photonentorpedos
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
// Energieversorgung
	
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	
//	Schilde
	
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	
//	Hülle
	
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	
// Lebenserhaltung
	
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	
// Androiden
	
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	
// Schiffsname
	
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return schiffsname;
	}
	
// Torpedo schießen	
	
	public void photonentorpedosSchießen(Raumschiff r) {
		Scanner tastatur = new Scanner (System.in);
		System.out.println("Photonentorpedo abschießen? [mit 'true' bestätigen]");
		boolean bestätigung = tastatur.nextBoolean();
		if (bestätigung = true) {
			System.out.println("-=ZIELERFASSUNG=-");
			System.out.println("Bitte Ziel auswählen. [r1, r2, r3]");
			String ziel = tastatur.next();
			
				if (photonentorpedoAnzahl > 0) {
					photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
					//treffer();
				}
			
		}
		else {
			System.out.println("-=VORGANG ABGEBROCHEN.=-");
		}
	}
	
	public void treffer(String ziel) {
		
	}
	
}
