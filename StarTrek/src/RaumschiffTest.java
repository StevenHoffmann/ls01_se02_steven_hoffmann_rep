
public class RaumschiffTest {

	public static void main(String[] args) {
		
		Raumschiff r1 = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff r2 = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff r3 = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		
		
		System.out.println("Klingonen-Raumschiff R1:");
		System.out.println("Anzahl Photonentopedos:		"+ r1.getPhotonentorpedoAnzahl());
		System.out.println("Integrit�t Energieversorgung: 	"+ r1.getEnergieversorgungInProzent());
		System.out.println("Integrit�t Schilde: 		"+ r1.getSchildeInProzent());
		System.out.println("Integrit�t H�lle:		"+ r1.getHuelleInProzent());
		System.out.println("Integrit�t Lebenserhaltung: 	"+ r1.getLebenserhaltungssystemeInProzent());
		System.out.println("Anzahl Androiden an Bord: 	"+ r1.getAndroidenAnzahl());
		System.out.println("Name des Schiffes: 		"+ r1.getSchiffsname());
		System.out.println();
		
		System.out.println("Romulaner-Raumschiff R2:");
		System.out.println("Anzahl Photonentorpedos 	"+ r2.getPhotonentorpedoAnzahl());
		System.out.println("Integrit�t Energieversorgung: 	"+ r2.getEnergieversorgungInProzent());
		System.out.println("Integrit�t Schilde: 		"+ r2.getSchildeInProzent());
		System.out.println("Integrit�t H�lle: 		"+ r2.getHuelleInProzent());
		System.out.println("Integrit�t Lebenserhaltung: 	"+ r2.getLebenserhaltungssystemeInProzent());
		System.out.println("Anzahl Androiden an Bord: 	"+ r2.getAndroidenAnzahl());
		System.out.println("Name des Schiffes: 		"+ r2.getSchiffsname());
		System.out.println();
		
		System.out.println("Vulkanier-Raumschiff R3:");
		System.out.println("Anzahl Photonentorpedos: 	"+ r3.getPhotonentorpedoAnzahl());
		System.out.println("Integrit�t Energieversorgung: 	"+ r3.getEnergieversorgungInProzent());
		System.out.println("Integrit�t Schilde: 		"+ r3.getSchildeInProzent());
		System.out.println("Integrit�t H�lle: 		"+ r3.getHuelleInProzent());
		System.out.println("Integrit�t Lebenserhaltung: 	"+ r3.getLebenserhaltungssystemeInProzent());
		System.out.println("Anzahl Androiden an Bord: 	"+ r3.getAndroidenAnzahl());
		System.out.println("Name des Schiffes: 		"+ r3.getSchiffsname());
		System.out.println();
	}
	
}
