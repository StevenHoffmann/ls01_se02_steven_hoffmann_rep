import java.util.Scanner;

public class Array�bung4 {

	public static void main(String[] args) {
		
		// AUFGABE 4 - AB Einfache Array Aufgaben
		
		int[] lotto = {3,7,12,18,37,42};
		
		for (int i = 0 ; i < lotto.length ; i++) {
			System.out.print(lotto[i] + " ");
		}	

		Scanner abfrage = new Scanner(System.in);
		System.out.println("");
		System.out.println("Nach welcher Zahl m�chten Sie suchen?");
		int abfragezahl = abfrage.nextInt();
		if (abfragezahl == lotto[0] || abfragezahl == lotto[1] || abfragezahl == lotto[2] ||
			abfragezahl == lotto[3] || abfragezahl == lotto[4] || abfragezahl == lotto[5]) {
			System.out.println("Die Zahl " + abfragezahl + " ist in der Ziehung enthalten.");
		}
		else {
			System.out.println("Die Zahl " + abfragezahl + " ist nicht in der Ziehung enthalten.");
		}
		
	}

}
