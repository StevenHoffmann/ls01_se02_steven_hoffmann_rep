import java.util.Scanner;

public class �bung4 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte den Startwert in Celsius eingeben:");
		int startwert = tastatur.nextInt();
		System.out.println("Bitte den Endwert in Celsius eingeben:");
		int endwert = tastatur.nextInt();
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben:");
		int schrittweite = tastatur.nextInt();
		double cwert = startwert;
		double fwert = 0;
		while (cwert <= endwert) {
			fwert = cwert * 1.8 + 32;
			System.out.printf("%.2f�C           %.2f�F%n", cwert, fwert);
			cwert = cwert + schrittweite;
		}
		
	}

}
