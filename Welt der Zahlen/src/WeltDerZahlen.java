/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 150000000000l ;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3769000 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 8150 ;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 190000 ;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int flaecheGroessteLand = 17098242 ;
    
    // Wie groß ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten in unserem Sonnensystem: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in der Milchstra�e: " + anzahlSterne);
    
    System.out.println("Anzahl der Einwohner von Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println("Gewicht des schwersten Tiers der Welt: " + gewichtKilogramm + " kg");
    
    System.out.println("Fl�che des gr��ten Lands der Welt: " + flaecheGroessteLand + "km�");
    
    System.out.println("Fl�che des kleinsten Lands der Welt: " + flaecheKleinsteLand + "km�");
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

