// Zur Vereinfachung mit ArrayList in Klasse ArrayListBeispiele 
public class Buch {

	private String titel;
	private double preis;

	public Buch(String titel, double preis) {
		this.titel = titel;
		this.preis = preis;

	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getTitel() {
		return titel;
	}
	
	public void setPreis(double preis) {
		this.preis = preis;
	}
	
	public double getPreis(double preis) {
		return preis;
	}

	@Override
	public String toString() {
		return "[ " + this.titel + " , " + this.preis + " ]";
	}
	
}
