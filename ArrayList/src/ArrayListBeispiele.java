import java.util.ArrayList;

public class ArrayListBeispiele {

	public static void main(String[] args) {
		
		ArrayList<String> namenliste = new ArrayList<String>();
		
		namenliste.add("Max");
		namenliste.add("Alex");
		namenliste.add("Anna");
		
		System.out.println(namenliste);
		
// Das erste Objekt in einem Array ist die 0. Deshalb 1 = Alex
		
		namenliste.remove(1);
		
		System.out.println(namenliste);

// ArrayList Klasse Buch
		
		ArrayList<Buch> buchliste = new ArrayList<Buch>();
		
		Buch b1 = new Buch("OOP", 20.99);
		
		buchliste.add(b1);
		
		System.out.println(buchliste);
		
// ArrayList Klasse Person
		
		ArrayList<Person> personenliste = new ArrayList<Person>();
		
		Person p1 = new Person("Tom", 25);
		Person p2 = new Person("Anna", 34);
		Person p3 = new Person("Hans", 32);
		
		personenliste.add(p1);
		personenliste.add(p2);
		personenliste.add(p3);
		
		System.out.println(personenliste);
		
		personenliste.remove(0);
		
		System.out.println(personenliste);
		
	}

}
