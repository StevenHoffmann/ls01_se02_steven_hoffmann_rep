import java.util.Scanner;

public class ArbeitsblattAufgabe3 {

	public static void main(String[] args) {
		
		double preisMause = preiserfassen();
		double anzahlMause = anzahlerfassen();
		double versandkosten = versandkostenrechnung(anzahlMause);
		double gesamtpreis = preisberechnung(preisMause,anzahlMause,versandkosten);
		System.out.printf("Ihr zu bezahlender Betrag liegt bei %.2f �.\n", gesamtpreis);
		System.out.println("Wir danken f�r Ihren Einkauf.");
		
	}

	public static double preiserfassen() {
		System.out.println("Wie viel kostet die von Ihnen gew�hlte Maus?");
		Scanner eingabe = new Scanner(System.in);
		double preisMause = eingabe.nextDouble();
		return preisMause;
	}
	
	public static double anzahlerfassen() {
		System.out.println("Wie viele M�use m�chten Sie kaufen?");
		Scanner eingabe = new Scanner(System.in);
		double anzahlMause = eingabe.nextDouble();
		return anzahlMause;
	}
	
	public static double versandkostenrechnung(double anzahlMause) {
		double versandkosten;
		
		if (anzahlMause < 10) {
			versandkosten = 10;
			return versandkosten;
		}
		else {
			versandkosten = 0;
			return versandkosten;
		}
	}
	
	public static double preisberechnung(double preisMause, double anzahlMause, double versandkosten) {
		double mwst = 1.19;
		double gesamtpreis = preisMause * anzahlMause * mwst + versandkosten;
		return gesamtpreis;
	}
	
}
