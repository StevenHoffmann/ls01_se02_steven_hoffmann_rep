import java.util.Scanner;

public class ArbeitsblattAufgaben {

	public static void main(String[] args) {
		
		// AUFGABE 2: STEUERSATZ
		
		double nettowert = nettowerterfassen();
		double steuersatz = steuersatzerfassen(nettowert);
		double bruttowert = bruttoberechnen(nettowert, steuersatz);
		
		System.out.println("Ihr Gesamtbetrag liegt bei " + bruttowert + "�.");
			
		}
		
	

	public static double nettowerterfassen() {
		System.out.println("Bitte geben Sie den Nettowert ein.");
		Scanner tastatur = new Scanner(System.in);
		double nettowert = tastatur.nextDouble();
		return nettowert;
	}
	
	public static double steuersatzerfassen(double nettowert) {
		double steuersatz;
		Scanner steuersatzeingabe = new Scanner(System.in);
		System.out.println("Wird f�r den Betrag von " + nettowert + "� der erm��igte Steuersatz angewendet?");
		System.out.println("Falls ja, geben Sie 'j' ein, falls nein, geben Sie 'n' ein.");
		String antwort;
		String steuer = steuersatzeingabe.next();
		if (steuer.equals("j")) {
			steuersatz = 107;
			return steuersatz;
		}
		else  {
			steuersatz = 119;
			return steuersatz;
		}
	
	}
	
	public static double bruttoberechnen(double nettowert, double steuersatz) {
		double bruttowert = nettowert * steuersatz/100;
		return bruttowert;
	}
	
}
