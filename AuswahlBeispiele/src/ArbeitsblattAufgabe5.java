import java.util.Scanner;

public class ArbeitsblattAufgabe5 {

	public static void main(String[] args) {
	
		// BMI-Rechner
		
		double gewicht = gewichterfassen();
		double gro�e = gro�eerfassen();
		double geschlecht = geschlechterfassen();
		double bmi = bmiberechnen(gewicht,gro�e);
		einordnung(bmi,geschlecht);
		
	}

	public static double gewichterfassen() {
		System.out.println("Bitte geben Sie Ihr K�rpergewicht in kg ein.");
		Scanner eingabe = new Scanner(System.in);
		double gewicht = eingabe.nextDouble();
		return gewicht;
	}
	
	public static double gro�eerfassen() {
		System.out.println("Bitte geben Sie Ihre K�rpergr��e in cm ein.");
		Scanner eingabe = new Scanner(System.in);
		double gro�ecm = eingabe.nextDouble();
		double gro�e = gro�ecm / 100;
		return gro�e;
	}
	
	public static double geschlechterfassen() {
		System.out.println("Welches Geschlecht haben Sie?");
		System.out.println("Geben Sie ein m/M f�r m�nnlich und w/W f�r weiblich.");
		Scanner eingabe = new Scanner(System.in);
		String geschlechteingabe = eingabe.next();
		
		if (geschlechteingabe.equals("m") || geschlechteingabe.equals("M")) {
			double geschlecht = 1;
			return geschlecht;
		}
		else {
			double geschlecht = 0;
			return geschlecht;
		}
	}
	
	public static double bmiberechnen(double gewicht, double gro�e) {
		double bmi = gewicht / (gro�e * gro�e);
		System.out.printf("Ihr BMI liegt bei: %.2f.\n" , bmi);
		return bmi;
	}
	
	public static void einordnung(double bmi, double geschlecht) {
		if (geschlecht == 1) {
			if (bmi < 20) {
				System.out.println("Sie haben Untergewicht.");
			}	
			if (bmi >= 20 && bmi <= 25) {
				System.out.println("Sie haben Idealgewicht.");
			}
			if (bmi > 25) {
				System.out.println("Sie haben �bergewicht.");
			}
			else {
				System.out.println(" ");
			}
		}
		else {
			if (bmi < 19) {
				System.out.println("Sie haben Untergewicht.");
			}	
			if (bmi >= 19 && bmi <= 24) {
				System.out.println("Sie haben Idealgewicht.");
			}
			if (bmi > 24) {
				System.out.println("Sie haben �bergewicht.");
			}
			else {
				System.out.println(" ");
			}
		}
	}
	
}
