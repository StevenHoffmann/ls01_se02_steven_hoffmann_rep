
public class Angestellter {

	private String name;
	private double gehalt;
	private int alter;

	public Angestellter() {
		this.name = "Unbekannt";
		this.gehalt = 0;
		this.alter = 0;
	}

	public Angestellter(String name, double gehalt, int alter) {
		this.name = name;
		this.gehalt = gehalt;
		this.alter = alter;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}

	public double getGehalt() {
		return gehalt;
	}

	public void setAlter(int alter) {
		this.alter = alter;
	}

	public int getAlter() {
		return alter;
	}

}
