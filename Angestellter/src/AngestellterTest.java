
public class AngestellterTest {

	public static void main(String[] args) {

		Angestellter a1 = new Angestellter();

		a1.setName("Max");
		a1.setGehalt(6500);
		a1.setAlter(58);

		System.out.println("Objekt A1:");
		System.out.println(a1.getName());
		System.out.println(a1.getGehalt());
		System.out.println(a1.getAlter());
		System.out.println();

		Angestellter a2 = new Angestellter("Alex", 5000.50, 45);

		System.out.println("Objekt A2:");
		System.out.println(a2.getName());
		System.out.println(a2.getGehalt());
		System.out.println(a2.getAlter());

	}
}
