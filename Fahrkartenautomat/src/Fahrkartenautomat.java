﻿import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {


        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double eingegebenerBetrag;
        
        
        double zuZahlenderGesamtbetrag = fahrkartenbestellungErfassen();
        double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderGesamtbetrag);
        fahrkartenAusgeben("\nFahrschein wird ausgegeben"); 
        rueckgeldAusgeben(rueckgabebetrag);
        wiederholung();
    
    }

// METHODE 5 - WIEDERHOLUNGSABRAFGE
    
    public static void wiederholung() {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println("Möchten Sie mehr Fahrkarten kaufen? [Ja/Nein]");
        String wiederholung = tastatur.next();
        if (wiederholung.equals("Ja") || wiederholung.equals("ja") || wiederholung.equals("JA")) {
          main(null);
        } else {
          System.exit(1);
        }
      }
    
// METHODE 1 - FAHRKARTENAUSWAHL
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	String[] ticketnamepool = {	"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC", 
    							"Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC",
    							"Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC",
    							"Kleingruppen-Tageskarte Berlin ABC"};
    	double[] ticketpreispool = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
    	System.out.println("==================================================");
    	for (int i = 0 ; i < ticketnamepool.length ; i++) {
			System.out.printf("(" + (i+1) +") " + ticketnamepool[i] + " [%.2f EUR]" + "%n", ticketpreispool[i]);
		}
    	int karte = tastatur.nextInt();
    	if(karte<1 || karte>10) {
    		System.out.println(" Der von Ihnen gewählte Fahrschein existiert nicht.");
    		fahrkartenbestellungErfassen();
    	}
    	else
    		System.out.printf("Ihre Wahl: (" + karte + "), " + ticketnamepool[karte-1] + " [%.2f EUR] pro Karte. \n", ticketpreispool[karte-1]);
    	
    		
    		System.out.println("Wie viele Tickets möchten Sie kaufen?:");
        int anzahlTickets = tastatur.nextInt();
        while(anzahlTickets < 1 || anzahlTickets > 10) {
        	anzahlTickets = 0;
        	System.out.println(">>>FEHLER<<<");
        	System.out.println("Der eingegebene Wert ist ungültig.");
        	System.out.println("Bitte geben Sie einen Wert zwischen 1 und 10 ein.");
        	System.out.println("Wie viele Tickets möchten Sie kaufen?");
        	anzahlTickets = tastatur.nextInt();
        }
        double ticketPreis = ticketpreispool[karte-1];
        double zuZahlenderGesamtbetrag = ticketPreis * anzahlTickets;
        return zuZahlenderGesamtbetrag;
    }
    
    

// METHODE 2 - GELDEINWURF
    
    public static double fahrkartenBezahlen(double zuZahlenderGesamtbetrag) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double rueckgabebetrag =0;
    	double eingeworfeneMuenze =0;
    	double[] geldmittel = {0.05, 0.10, 0.20, 0.50, 1, 2};
    	int pruefung = 0;
    	
    	while (eingezahlterGesamtbetrag < zuZahlenderGesamtbetrag) {
    		System.out.format("Noch zu zahlen: %4.2f €%n", (zuZahlenderGesamtbetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            Scanner tastatur = new Scanner(System.in);
            eingeworfeneMuenze = tastatur.nextDouble();
            for (int i = 0 ; i < geldmittel.length ; i++) {
            	if (geldmittel[i] == eingeworfeneMuenze) {
            		pruefung = 1;
            	}
            }
            if(pruefung==0) {
            	System.out.println("Werfen Sie ein gültiges Zahlungsmittel ein.");
            }
            else {
            	pruefung=0;
            	eingezahlterGesamtbetrag += eingeworfeneMuenze;
        		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderGesamtbetrag;
            }
    	}	
            
            
            
            
/*            for (int i = 0 ; i < geld.length ; i++) {
            
    			if(eingeworfeneMuenze != geld[i]) {
                	u = u+1;
    			}

            }
            if(u!=5) {
            	System.out.println("*****Bitte werfen Sie ein gültiges Geldmittel ein.*****");
            }
            else {
            	eingezahlterGesamtbetrag += eingeworfeneMuenze;
        		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderGesamtbetrag;
            }
        }
*/    	
    	return rueckgabebetrag;
    	
    }
    
// METHODE 3 - FAHRKARTENAUSGABE
    
    public static void fahrkartenAusgeben(String text) {
    	System.out.println(text);
    	for (int i = 0; i < 30; i++) {
            System.out.print("=");
            try {
               Thread.sleep(75);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            }
    	System.out.print("\n\n");
    }
    
// METHODE 4 - RÜCKGELDAUSGABE
    
    public static void rueckgeldAusgeben (double rueckgabeBetrag) {
    	if (rueckgabeBetrag > 0.00) {
    		System.out.format("Der Rückgabebetrag in Höhe von %4.2f € %n", rueckgabeBetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

                while (rueckgabeBetrag >= 2.0)
               { 
            	   System.out.println("2 EURO");
                   rueckgabeBetrag -= 2.0;
               }
               while (rueckgabeBetrag >= 1.0)
               {	   
            	   System.out.println("1 EURO");
            	   rueckgabeBetrag -= 1.0;
               }
               while (rueckgabeBetrag >= 0.50)
               {
                   System.out.println("50 CENT");
                   rueckgabeBetrag -= 0.50;
               }
               while (rueckgabeBetrag >= 0.20)
               {
                   System.out.println("20 CENT");
                   rueckgabeBetrag -= 0.20;
               }
               while (rueckgabeBetrag >= 0.09)
               {
                   System.out.println("10 CENT");
                   rueckgabeBetrag -= 0.10;
               }
               while (rueckgabeBetrag >= 0.04)
               {
                   System.out.println("5 CENT");
                   rueckgabeBetrag -= 0.05;
               }
               
        }
            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                    + "Wir wünschen Ihnen eine gute Fahrt.");
            System.out.println("====================");
    }
    
}
    
  
    
   