import java.util.Scanner;
public class MethodenBeispiele {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner myScanner = new Scanner(System.in);
		
		//System.out.println("Geben Sie Ihren Namen ein:");
		//String name = myScanner.next();
		
		String vname = leseString("Geben Sie bitte Ihren Vornamen ein:");
		String nname = leseString("Geben Sie bitte Ihren Nachnamen ein:");
		sayHello(vname,nname);
		
		int alter = leseInt("Geben Sie bitte Ihr Alter ein:");
		System.out.println("Alter: " + alter);
		

	}
	
	public static String leseString(String text) {
		System.out.println(text);
		Scanner myScanner = new Scanner(System.in);
		
		String str = myScanner.next();
		
		return str;
		
	}

	public static int leseInt(String zahl) {
		System.out.println(zahl);
		Scanner myScanner = new Scanner(System.in);
		
		int ergebnis = myScanner.nextInt();
		return ergebnis;
		
	}
	
	public static void sayHello(String vname, String nname) {
		System.out.println("hello "+ vname + " " + nname);
	}
	
	public static void sayHello() {
		System.out.println("hello Max");
	}
}
